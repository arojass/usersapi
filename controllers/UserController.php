<?php

namespace app\controllers;

use app\models\User;
use Exception;
use sizeg\jwt\Jwt;
use sizeg\jwt\JwtHttpBearerAuth;
use stdClass;
use Yii;
use yii\filters\ContentNegotiator;
use yii\helpers\Json;
use yii\httpclient\Client;
use yii\rest\ActiveController;
use yii\web\Response;

class UserController extends ActiveController
{
    public $modelClass = 'app\models\User';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => ContentNegotiator::class,
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                    'application/xml' => Response::FORMAT_XML,
                ],
            ],
            'authenticator' => [
                'class' => JwtHttpBearerAuth::class,
                'only' => ['login', 'new', 'me'],
                'optional' => ['login', 'new'],
            ]
        ];
    }

    public function actionMe()
    {
        //Inicializamos la clase estándar para las respuestas.
        $response = new stdClass();
        //Inicializamos los parámetros de la respuesta por defecto.
        $response->code = "2100";
        $response->message = [];

        //Al tratarse de un API externa, realizamos las solicitudes a través de cliente http.
        $client = new Client([
            'baseUrl' => Yii::$app->params["mockApiUrl"]
        ]);

        if (Yii::$app->request->isGet) {

            //Decodificamos el JSON en data.
            try {
                $data = JSON::decode(file_get_contents('php://input'), true);
            } catch (Exception $e) {
                $response->code = "2200";
                $response->message = "Problemas al decodificar los datos del Json. Error: " . $e->getMessage();
                return $response;
            }

            //Validamos que data no esté vacío y que contenga el campo ID.
            if (empty($data) || !array_key_exists('id', $data)) $response->message['id'] = "El parámetro id no puede estar vacío.";
            if (!empty($response->message)) return $response;

            //Obtenemos la identidad del User que está realizando la solicitud.
            $identity = Yii::$app->user->identity;

            //Validamos si el id del User a buscar es el mismo del User que se ha identificado.
            if ($identity->getId() !== $data['id']) {
                $response->code = "2400";
                $response->message = "Acceso denegado. Usted no posee permisos para realizar esta acción.";
                return $response;
            } else {
                return Yii::$app->user->identity; //Si es el mismo, retornamos los datos del usuario autenticado.
            }
        } elseif (Yii::$app->request->isPut) {

            //Decodificamos el JSON en data.
            try {
                $data = JSON::decode(file_get_contents('php://input'), true);
            } catch (Exception $e) {
                $response->code = "2200";
                $response->message = "Problemas al decodificar los datos del Json. Error: " . $e->getMessage();
                return $response;
            }

            //Validamos que data no esté vacío y que contenga el campo ID (para actualizar el valor, no requerimos ningún campo obligatorio)
            if (empty($data) || !array_key_exists('id', $data)) $response->message['id'] = "El parámetro id no puede estar vacío.";
            if (!empty($response->message)) return $response;

            //Eliminamos los parámetros sensibles que pueden venir en la solicitud.
            unset($data['auth_key']);
            unset($data['access_token']);
            unset($data['password']);

            //Obtenemos la identidad del User que está realizando la solicitud.
            $identity = Yii::$app->user->identity;

            //Validamos si el id del User a buscar es el mismo del User que se ha identificado.
            if ($identity->getId() !== $data['id']) {
                $response->code = "2400";
                $response->message = "Acceso denegado. Usted no posee permisos para realizar esta acción.";
                return $response;
            }

            //Creamos un objeto del tipo User para almacenar los datos
            $user = new User();

            //Asignamos la información de data al User
            $user->setAttributes($data, false);

            //Si el User es actualizado correctamente, retorna sus datos, de lo contrario arrojará el mensaje del error.
            try {
                $response = $client->put(
                    'users/' . $user->id,
                    $user->attributes
                )->send();
                $user->setAttributes($response->data, false);
                return $user;
            } catch (\yii\httpclient\Exception $e) {
                return $e->getMessage();
            }

        } elseif (Yii::$app->request->isDelete) {

            //Decodificamos el JSON en data.
            try {
                $data = JSON::decode(file_get_contents('php://input'), true);
            } catch (Exception $e) {
                $response->code = "2200";
                $response->message = "Problemas al decodificar los datos del Json. Error: " . $e->getMessage();
                return $response;
            }

            //Validamos que data no esté vacío y que contenga el campo ID.
            if (empty($data) || !array_key_exists('id', $data)) $response->message['id'] = "El parámetro id no puede estar vacío.";
            if (!empty($response->message)) return $response;

            //Obtenemos la identidad del User que está realizando la solicitud.
            $identity = Yii::$app->user->identity;

            //Validamos si el id del User a buscar es el mismo del User que se ha identificado.
            if ($identity == null || $identity->getId() !== $data['id']) {
                $response->code = "2400";
                $response->message = "Acceso denegado. Usted no posee permisos para realizar esta acción.";
                return $response;
            }

            //Si el User existe y se puede eliminar, retornamos True, de lo contrario arrojará el mensaje del error.
            try {
                $response = $client->delete(
                    'users/' . $data['id']
                )->send();
                return $response->isOk;
            } catch (\yii\httpclient\Exception $e) {
                return $e->getMessage();
            }

        } elseif (Yii::$app->request->isOptions) {
            $response->code = "2300";
            $response->message = "This endpoint just support GET and PUT actions.";
            return $response;
        } else {
            $response->code = "2300";
            $response->message = "This endpoint just support GET and PUT actions.";
            return $response;
        }
    }

    public function actionNew()
    {
        //Inicializamos la clase estándar para las respuestas.
        $response = new stdClass();
        //Inicializamos los parámetros de la respuesta por defecto.
        $response->code = "2100";
        $response->message = [];

        if (Yii::$app->request->isPost) {

            //Decodificamos el JSON en data.
            try {
                $data = JSON::decode(file_get_contents('php://input'), true);
            } catch (Exception $e) {
                $response->code = "2200";
                $response->message = "Problemas al decodificar los datos del Json. Error: " . $e->getMessage();
                return $response;
            }

            //Validamos que data no esté vacío y que contenga los campos necesarios para crear el usuario.
            if (empty($data) || !array_key_exists('name', $data)) $response->message['name'] = "El parámetro name no puede estar vacío.";
            if (empty($data) || !array_key_exists('email', $data)) $response->message['email'] = "El parámetro email no puede estar vacío.";
            if (empty($data) || !array_key_exists('password', $data)) $response->message['password'] = "El parámetro password no puede estar vacío.";
            if (empty($data) || !array_key_exists('avatar', $data)) $response->message['avatar'] = "El parámetro avatar no puede estar vacío.";
            if (!empty($response->message)) return $response;

            //Al tratarse de un API externa, realizamos las solicitudes a través de cliente http.
            $client = new Client([
                'baseUrl' => Yii::$app->params["mockApiUrl"]
            ]);

            try {

                //Creamos el User
                $reply_post = $client->post(
                    'users',
                    [
                        'name' => $data['name'],
                        'email' => $data['email'],
                        'password' => crypt($data['password'], Yii::$app->params["salt"]),
                        'avatar' => $data['avatar'],
                        'auth_key' => SiteController::randKey("abcdef0123456789-", 36),
                    ]
                )->send();

                /** @var Jwt $jwt */
                $jwt = Yii::$app->jwt;
                $signer = $jwt->getSigner('HS256');
                $key = $jwt->getKey();
                $time = time();

                $token = $jwt->getBuilder()
                    ->issuedBy('http://localhost:8080')
                    ->permittedFor('http://localhost:8080')
                    ->identifiedBy('5gaH76Ajul9', true)
                    ->issuedAt($time)
                    ->expiresAt($time + (3600 * 24))
                    ->withClaim('uid', $reply_post->data['id'])
                    ->getToken($signer, $key);

                //Actualizamos su access_token
                $reply_put = $client->put(
                    'users/' . $reply_post->data['id'],
                    ['access_token' => (string)$token]
                )->send();

                //Retornamos el id del User y el token generado
                return $this->asJson([
                    'id' => $token->getClaim('uid'),
                    'token' => $reply_put->data['access_token'],
                ]);

            } catch (\yii\httpclient\Exception $e) {
                return "\yii\httpclient\Exception" . $e->getMessage();
            }
        } elseif (Yii::$app->request->isOptions) {
            $response->code = "2300";
            $response->message = "This endpoint just support POST action.";
            return $response;
        } else {
            $response->code = "2300";
            $response->message = "This endpoint just support POST action.";
            return $response;
        }
    }

    public function actionLogin()
    {
        //Inicializamos la clase estándar para las respuestas.
        $response = new stdClass();
        //Inicializamos los parámetros de la respuesta por defecto.
        $response->code = "2100";
        $response->message = [];

        if (Yii::$app->request->isPost) {

            //Decodificamos el JSON en data.
            try {
                $data = JSON::decode(file_get_contents('php://input'), true);
            } catch (Exception $e) {
                $response->code = "2200";
                $response->message = "Problemas al decodificar los datos del Json. Error: " . $e->getMessage();
                return $response;
            }

            //Validamos que data no esté vacío y que contenga los campos necesarios para identificar el usuario.
            if (empty($data) || !array_key_exists('email', $data)) $response->message['email'] = "El parámetro email no puede estar vacío.";
            if (empty($data) || !array_key_exists('password', $data)) $response->message['password'] = "El parámetro password no puede estar vacío.";
            if (!empty($response->message)) return $response;

            $user = new User();

            //Al tratarse de un API externa, realizamos las solicitudes a través de cliente http.
            $client = new Client([
                'baseUrl' => Yii::$app->params["mockApiUrl"]
            ]);

            try {
                //Obtenemos el User
                $reply_get = $client->get(
                    'users',
                    ['email' => $data['email']]
                )->send();

                if (!empty($reply_get->data)) $user->setAttributes($reply_get->data[0], false);

                //Validamos el password
                if (!$user->validatePassword($data['password'])) {
                    $response->code = "2500";
                    $response->message = "El correo o contraseña no son válidos.";
                    return $response;
                }

                /** @var Jwt $jwt */
                $jwt = Yii::$app->jwt;
                $signer = $jwt->getSigner('HS256');
                $key = $jwt->getKey();
                $time = time();

                $token = $jwt->getBuilder()
                    ->issuedBy('http://localhost:8080')
                    ->permittedFor('http://localhost:8080')
                    ->identifiedBy('5gaH76Ajul9', true)
                    ->issuedAt($time)
                    ->expiresAt($time + (3600 * 24))
                    ->withClaim('uid', $user->id)
                    ->getToken($signer, $key);

                //Actualizamos su access_token
                $reply_put = $client->put(
                    'users/' . $user->id,
                    ['access_token' => (string)$token]
                )->send();

                //Retornamos el token actualizado
                return $this->asJson([
                    'id' => $token->getClaim('uid'),
                    'token' => $reply_put->data['access_token'],
                ]);

            } catch (Exception $e) {
                return $e->getMessage();
            }
        } elseif (Yii::$app->request->isOptions) {
            $response->code = "2300";
            $response->message = "This endpoint just support POST action.";
            return $response;
        } else {
            $response->code = "2300";
            $response->message = "This endpoint just support POST action.";
            return $response;
        }
    }
}