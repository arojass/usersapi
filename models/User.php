<?php

namespace app\models;

use Exception;
use Lcobucci\JWT\Token;
use Yii;
use yii\base\Model;
use yii\httpclient\Client;
use yii\web\IdentityInterface;

class User extends Model implements IdentityInterface
{
    public $id;
    public $email;
    public $password;
    public $name;
    public $avatar;
    public $auth_key;
    public $access_token;
    public $createdAt;

    public function fields()
    {
        $fields = parent::fields();
        unset($fields['auth_key'], $fields['password'], $fields['access_token']);
        return $fields;
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|int $id the ID to be looked for
     * @return IdentityInterface|null the identity object that matches the given ID.
     */
    public static function findIdentity($id)
    {
        $client = new Client([
            'baseUrl' => Yii::$app->params["mockApiUrl"]
        ]);

        try {
            $response = $client->get(
                'users',
                ['id' => $id]
            )->send();

            if ($response->data[0]['id'] !== $id) return null;

            $identity = new User();
            $identity->setAttributes($response->data[0], false);
            return $identity;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * Finds an identity by the given token.
     *
     * @param Token $token the token to be looked for
     * @param string|null $type the type of the token, depends of the implementation.
     * @return IdentityInterface|null the identity object that matches the given token.
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $client = new Client([
            'baseUrl' => Yii::$app->params["mockApiUrl"]
        ]);

        try {
            $response = $client->get(
                'users',
                ['id' => $token->getClaim('uid')]
            )->send();

            if (!empty($reply->data)) return null;

            $identity = new User();
            $identity->setAttributes($response->data[0], false);
            return $identity;

        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @return int|string current user ID
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string current user auth key
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @param string $authKey
     * @return bool if auth key is valid for current user
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validate the password given
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        if (crypt($password, $this->password) == $this->password) {
            return $password === $password;
        }
    }
}
