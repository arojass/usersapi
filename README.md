<p align="center">
    <h1 align="center">Users API</h1>
    <br>
</p>

Este proyecto fue creado para la evaluación técnica de la empresa Haulmer.

ESTRUCTURA DEL DIRECTORIO UTILIZADO
-------------------
      config/             contiene archivos de configuración
      controllers/        contiene el controlador de User
      models/             contiene el modelo de User
      vendor/             contiene los archivos de terceras partes utilizados

El resto de los directorios pertenecen al framework, pero no fueron utilizados.

REQUISITOS
------------

- El requerimiento mínimo para utilizar este proyecto es PHP 5.6.0 (preferible PHP 7.2.0 o superior)
- Contar con [Composer](http://getcomposer.org/) instalado en el equipo. Si no lo tienes, puedes seguir 
  las siguientes instrucciones [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix) 
  para instalarlo.

INSTALACIÓN
------------

Clonar el proyecto desde el repositorio alojado en [Bitbucket](https://bitbucket.org/).

~~~
git clone https://arojass@bitbucket.org/arojass/usersapi.git
~~~

A través de una consola o terminal, dirigirse a la raíz del proyecto.

~~~
/path/to/project/userapi
~~~

Una vez en la raíz del directorio, debemos instalar/actualizar las dependencias con Composer.

~~~
composer install (o composer update)
~~~

Finalmente, iniciamos el servidor web.

~~~
./yii serve
~~~

SOBRE EL PROYECTO
------------

Información general

- Para el desarrollo del presente proyecto se utilizó el framework de PHP [Yii2](https://www.yiiframework.com) en su versión del proyecto "basic".
- Para la persistencia de los datos se utilizó una API externa creada en [mockapi.io](https://www.mockapi.io).

Información del proyecto

- Para la autenticación con JWT se utilizó la librería [yii2-jwt](https://github.com/sizeg/yii2-jwt).
- La URL del API de mockapi.io utilizada se encuentra almacenada en el archivo *config/params.php*.
- La configuración de los endpoints habilitados se encuentra en el archivo *config/web*.
- El controlador y modelo utilizados en este servicio corresponden a *controllers/UserController.php* y *models/User.php* respectivamente.

DOCUMENTACIÓN DEL API
------------

La documentación del API desarrollada a través de este proyecto la puedes encontrar en el siguiente link de [Postman](https://www.postman.com):

~~~
https://documenter.getpostman.com/view/2194754/TVsoJWV4
~~~

Desarrollado por [Alex Rojas](https://www.linkedin.com/in/airojass/).